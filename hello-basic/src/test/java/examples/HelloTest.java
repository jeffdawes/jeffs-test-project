package examples;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test personalised hello message
 * 
 * @author louiseelliott
 *
 */
class HelloTest {
	private HelloWithLogging h;
	
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeEach
	void setUp() throws Exception {
		h = new HelloWithLogging();
	}

	/**
	 * Test method for {@link examples.HelloWithLogging#getMessage()}.
	 */
	@Test
	void testGetMessage() {
		h.setName("World");
		assertEquals(h.getName(),"World");
		assertEquals(h.getMessage(),"Hello World!");
	}
	
	/**
	 * Test method for {@link examples.HelloWithLogging#getMessage()}.
	 */
	@Test
	void testGetMessageWithNull() {
		h.setName(null);
		assertEquals(h.getName(),"");
		assertEquals(h.getMessage(),"Hello !");
	}

}
