/**
 * 
 */
package examples;

import org.apache.log4j.Logger;

/**
 * Return personalised Hello message - includes logging
 * 
 * @author louiseelliott
 *
 */
public class HelloWithLogging {
	// Define a logger for logging a message
	private final static Logger logger = Logger.getLogger(HelloWithLogging.class);

	// name to be used in hello message
	private String name = "";

	/**
	 * Show log4j messages based on log priority
	 */
	public HelloWithLogging() {
		super();
		this.logTest("Test log message priority");
	}

	/**
	 * get hello message name
	 * 
	 * @return name
	 */
	public String getName() {
		logger.debug("In getName() => " + name);
		return name;
	}

	/**
	 * set hello message name
	 * 
	 * @param name - used to personalise message
	 */
	public void setName(String name) {
		logger.debug("In setName('" + name + "')");

		if (name != null)
			this.name = name;
		else {
			this.name = "";
			logger.warn("In setName() => ignore and initialise name");
		}
	}

	/**
	 * display message name
	 * 
	 * @return message
	 */
	public String getMessage() {
		logger.debug("In getMessage()");
		String message = "Hello " + getName() + "!";
		logger.debug("getMessage() => " + message);

		return message;
	}

	/**
	 * Display log message depending on log priority
	 * 
	 * @param logMessage - string to append to log message
	 */
	private void logTest(String logMessage) {
		logger.debug("In logTest('" + logMessage + "')");
		if (logger.isDebugEnabled()) {
			logger.debug("This is debug : " + logMessage);
		}

		if (logger.isInfoEnabled()) {
			logger.info("This is info : " + logMessage);
		}

		logger.warn("This is warn : " + logMessage);
		logger.error("This is error : " + logMessage);
		logger.fatal("This is fatal : " + logMessage);
	}

}
