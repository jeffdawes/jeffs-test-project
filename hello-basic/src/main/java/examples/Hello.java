/**
 * 
 */
package examples;

/**
 * Return personalised Hello message
 * 
 * @author louiseelliott
 *
 */
public class Hello {

	private String name = "";

	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMessage() {
		return "Hello by Jeff 2nd try" + getName() + "!";
	}

}
